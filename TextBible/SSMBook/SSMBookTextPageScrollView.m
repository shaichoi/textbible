//
//  SSMBookTextPageScrollView.m
//  TextBible
//
//  Created by Seongwon Choi on 12. 5. 17..
//  Copyright (c) 2012년 ssmepub. All rights reserved.
//

#import "SSMBookTextPageScrollView.h"

#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet.h"

#import "FMDatabaseQueue.h"

@implementation SSMBookTextPageScrollView

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

@end
