//
//  SSMBookViewController.h
//  TextBible
//
//  Created by Seongwon Choi on 12. 5. 17..
//  Copyright (c) 2012년 ssmepub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSMBookSetting.h"

#import "SSMBookTextScrollView.h"
#import "SSMBookTextPageScrollView.h"

@interface SSMBookViewController : UIViewController {
    IBOutlet UIView *popupView;
    
    SSMBookTextScrollView *_bookTextSV;
    SSMBookTextPageScrollView *_bookTextPageSV;
}

@end
