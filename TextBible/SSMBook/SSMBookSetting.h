#import <Foundation/Foundation.h>

/*********************
 Do NOT edit follows.
 ******************** X */

#define SSM_BOOK_UNIT_SCROLLTYPE 10000

/*********************
 Can edit follows.
 ******************** O */

#define SSM_BOOK_TYPE_TEXT TRUE
#define SSM_BOOK_TYPE_IMAGE FALSE

#define SSM_DB_FILENAME @"temp.sqlite"

/*********************
 Do NOT edit follows.
 ******************** X */

extern NSString *const ssmBookCopyright;

typedef enum SSMBookScrollType {
	ssmBookScrollTypeHorizontalScroll = 10001,
	ssmBookScrollTypeVerticalScroll,
    ssmBookScrollTypeHorizontalPage,
    ssmBookScrollTypeVerticalPage
} SSMBookScrollType;

