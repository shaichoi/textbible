//
//  SSMBookViewController.m
//  TextBible
//
//  Created by Seongwon Choi on 12. 5. 17..
//  Copyright (c) 2012년 ssmepub. All rights reserved.
//

#import "SSMBookViewController.h"

@interface SSMBookViewController ()

@end

@implementation SSMBookViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        CGRect frameInitSize = CGRectMake(0.0, 0.0, 320.0, 460.0);
        _bookTextSV = [[SSMBookTextScrollView alloc] initWithFrame:frameInitSize];
        _bookTextPageSV = [[SSMBookTextPageScrollView alloc] initWithFrame:frameInitSize];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    popupView.frame = CGRectMake(0.0, 0.0, popupView.frame.size.width, popupView.frame.size.height);
    [self.view addSubview:popupView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc{
    [popupView release];
    
    [_bookTextSV release];
    [_bookTextPageSV release];
    
    [super dealloc];
}


@end
