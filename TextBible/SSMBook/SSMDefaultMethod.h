//
//  SSMDefaultMethod.h
//  TextBible
//
//  Created by Seongwon Choi on 12. 5. 18..
//  Copyright (c) 2012년 ssmepub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet.h"

@interface SSMDefaultMethod : NSObject{
    
}

+ (NSString *)createEditableCopyOfFileIfNeeded:(NSString *)_filename toCacheOrDocument:(NSString *)_toType;
//+ (UIImage *)imageFromDBWithType:(NSString *)type category:(int)category number:(int)number;
+ (BOOL)isUpdated:(NSString *)_targetFilename withUpdateDataFile:(NSString *)_updateDataFilename;
+ (NSString *)updateFile:(NSString *)_filename;
//+ (NSMutableArray *)arrayFromDBWithPage:(NSString *)page;

@end
