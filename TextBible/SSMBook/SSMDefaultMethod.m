//
//  SSMDefaultMethod.m
//  TextBible
//
//  Created by Seongwon Choi on 12. 5. 18..
//  Copyright (c) 2012년 ssmepub. All rights reserved.
//

#import "SSMDefaultMethod.h"

@implementation SSMDefaultMethod

+ (NSString *)createEditableCopyOfFileIfNeeded:(NSString *)_filename toCacheOrDocument:(NSString *)_toType{
    // 파일 존재 여부 확인.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
	
    NSArray *paths = nil;
    if([[_toType lowercaseString] isEqualToString:@"cache"]){
        paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    }
    else if([[_toType lowercaseString] isEqualToString:@"document"]){
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    }
    else{
        return nil;
    }
    NSString *directory = [paths objectAtIndex:0];
    NSString *writableFilePath = [[directory stringByAppendingPathComponent:_filename] copy];
	
    success = [fileManager fileExistsAtPath:writableFilePath];
    if (success) return writableFilePath;
	
	// 사용자의 Documents 디렉토리에 파일이 없는 경우, 앱의 resources 디렉토리에서 기본 데이터 파일을 가져온다.
	NSString *defaultFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:_filename];
    success = [fileManager copyItemAtPath:defaultFilePath toPath:writableFilePath error:&error];
    if (!success) {
		NSLog(@"%@", [error localizedDescription]);
        NSAssert1(0, @"Failed to create writable file with message '%@'.", [error localizedDescription]);
    }
	return writableFilePath;
}
/*
+ (UIImage *)imageFromDBWithType:(NSString *)type category:(int)category number:(int)number{
//    NSString *filePath = [DefaultMethod createEditableCopyOfFileIfNeeded:[NSString stringWithString:BOOK_DATA_SQLITE_FILENAME]];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:BOOK_DATA_SQLITE_FILENAME ofType:@""];
    FMDatabase *db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open DB. (*.sqlite)");
        return NO;
    }
    
    FMResultSet *s;
    NSData *tempData = nil;
    if([type isEqualToString:@"read"]){
        s = [db executeQuery:@"SELECT ImgData FROM img_read WHERE ImgNameCategory = ? AND ImgNameNumber = ?", [NSNumber numberWithInt:category], [NSNumber numberWithInt:number]];
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return nil;
        }
        while ([s next]) {
            tempData = [s dataForColumn:@"ImgData"];
            if(tempData == nil){
                NSLog(@"No image found.");
            }
        }
        return [UIImage imageWithData:tempData];
    }
    else if([type isEqualToString:@"gallery"] && category == 0){
        s = [db executeQuery:@"SELECT ImgData FROM img_gallery WHERE ImgNameNumber = ?", [NSNumber numberWithInt:number]];
        if ([db hadError]) {
            NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
            return nil;
        }
        while ([s next]) {
            tempData = [s dataForColumn:@"ImgData"];
            if(tempData == nil){
                NSLog(@"No image found.");
            }
        }
        return [UIImage imageWithData:tempData];
    }
    return nil;
}
*/

+ (BOOL)isUpdated:(NSString *)_targetFilename withUpdateDataFile:(NSString *)_updateDataFilename{
    BOOL isThereUpdataFilename = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filepath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:_updateDataFilename];
    isThereUpdataFilename = [fileManager fileExistsAtPath:filepath];
    
    if(isThereUpdataFilename){
        BOOL equalVersion = NO;
        
        NSString *targetFilepath = [DefaultMethod createEditableCopyOfFileIfNeeded:[NSString stringWithString:_targetFilename]];
        NSMutableDictionary *plistDict = [[NSMutableDictionary alloc] initWithContentsOfFile:filepath];
        NSMutableDictionary *plistTargetDict = [[NSMutableDictionary alloc] initWithContentsOfFile:targetFilepath];
        NSString *keyStr = @"version";
        NSString *tempStr = [plistDict objectForKey:keyStr];
        NSString *tempTargetStr = [plistTargetDict objectForKey:keyStr];
        if([tempStr isEqualToString:tempTargetStr]){
            equalVersion = YES;
        }
        else{
            equalVersion = NO;
        }
        tempStr = nil;
        tempTargetStr = nil;
        [plistDict release];
        [plistTargetDict release];
        
        if(equalVersion){
            NSLog(@"'%@' updated? %@", _targetFilename, equalVersion?@"YES":@"NO");
            return YES;
        }
        else{
            NSLog(@"'%@' updated? %@", _targetFilename, equalVersion?@"YES":@"NO");
            return NO;
        }
    }
    else{
        NSLog(@"'%@' updated? %@", _targetFilename, isThereUpdataFilename?@"YES":@"NO");
        return NO;
    }
}

+ (NSString *)updateFile:(NSString *)_filename{
    BOOL success;
    BOOL success_err;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [[documentsDirectory stringByAppendingPathComponent: _filename] copy];
	
    success = [fileManager fileExistsAtPath:filePath];
    if(success){ // 파일이 있다면,
        success_err = [fileManager removeItemAtPath:filePath error:&error];
        if (!success_err) {
            NSLog(@"%@", [error localizedDescription]);
            NSAssert1(0, @"Failed to delete the file with message '%@'.", [error localizedDescription]);
            return nil;
        }
    }
    
    NSString *defaultFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: _filename];
    success = [fileManager copyItemAtPath:defaultFilePath toPath:filePath error:&error];
    if (!success) {
		NSLog(@"%@", [error localizedDescription]);
        NSAssert1(0, @"Failed to create writable file with message '%@'.", [error localizedDescription]);
        return nil;
    }
    
    NSLog(@"'%@' to update: YES", _filename);
    return filePath;
}
/*
+ (NSMutableArray *)arrayFromDBWithPage:(NSString *)page{
    //    NSString *filePath = [DefaultMethod createEditableCopyOfFileIfNeeded:[NSString stringWithString:BOOK_DATA_SQLITE_FILENAME]];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:BOOK_DATA_SQLITE_FILENAME ofType:@""];
    FMDatabase *db = [FMDatabase databaseWithPath:filePath];
    if (![db open]) {
        NSLog(@"Could not open DB. (*.sqlite)");
        return NO;
    }
    
    FMResultSet *s;
    NSMutableArray *tempAry = nil;
    NSString *tempStr1 = nil;
    NSString *tempStr2 = nil;
    
    s = [db executeQuery:@"SELECT ImgNameCategory, ImgNameNumber FROM img_read WHERE ID = ?", [NSNumber numberWithInt:[page intValue]]];
    if ([db hadError]) {
        NSLog(@"Err %d: %@", [db lastErrorCode], [db lastErrorMessage]);
        return nil;
    }
    while ([s next]) {
        tempStr1 = [s stringForColumn:@"ImgNameCategory"];
        tempStr2 = [s stringForColumn:@"ImgNameNumber"];

        
        if([tempStr1 intValue] < 10){
            tempStr1 = [NSString stringWithFormat:@"0%@", tempStr1];
        }
        if([tempStr2 intValue] < 10){
            tempStr2 = [NSString stringWithFormat:@"000%@", tempStr2];
        }
        else if([tempStr2 intValue] >= 10 && [tempStr2 intValue] < 100){
            tempStr2 = [NSString stringWithFormat:@"00%@", tempStr2];
        }
        else if([tempStr2 intValue] >= 100 && [tempStr2 intValue] < 1000){
            tempStr2 = [NSString stringWithFormat:@"0%@", tempStr2];
        }
         
        
        tempAry = [NSMutableArray arrayWithObjects:tempStr1, tempStr2, nil];
        if(tempStr1 == nil || tempStr2 == nil){
            NSLog(@"No String data found.");
        }
    }
    
    return tempAry;
}
 */


@end
